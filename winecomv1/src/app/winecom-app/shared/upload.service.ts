import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  formTemplate: FormGroup;
  imageDetailList: AngularFireList<any>;

  imageAMano: AngularFireList<any>;
  imageHardysAndSons: AngularFireList<any>;
  imageYellowTail: AngularFireList<any>;
  imageBarefoot: AngularFireList<any>;
  imageConchaToro: AngularFireList<any>;
  imageGallo: AngularFireList<any>;
  imageOthers: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase,
    private fb: FormBuilder,
    private imageDelete: AngularFireStorage) {

    this.formTemplate = this.fb.group({
      $key: [null],
      caption: ['test', Validators.required],
      category: [''],
      hiddenImage: [''],
      imageUrl: ['', Validators.required],
      description: [''],
      allergen: [''],
      vintage: [''],
      country: [''],
      region: [''],
      type: [''],
      alcohol: [''],
    });
  }
  getImageDetailList() {
    this.imageDetailList = this.firebase.list('imageDetails');
    this.imageAMano = this.firebase.list('aMano');
    this.imageHardysAndSons = this.firebase.list('hardy');
    this.imageYellowTail = this.firebase.list('tail');
    this.imageBarefoot = this.firebase.list('foot');
    this.imageConchaToro = this.firebase.list('toro');
    this.imageGallo = this.firebase.list('gallo');
    this.imageOthers = this.firebase.list('others');

    return this.imageYellowTail.snapshotChanges();
  }

  getImageHardys() {
    return this.imageHardysAndSons.snapshotChanges();
  }
  getImageAMano() {
    return this.imageAMano.snapshotChanges();
  }
  getImageBarefoot() {
    return this.imageBarefoot.snapshotChanges();
  }
  getImageConchaToro() {
    return this.imageConchaToro.snapshotChanges();
  }
  getImageGallo() {
    return this.imageGallo.snapshotChanges();
  }
  getImageOthers() {
    return this.imageOthers.snapshotChanges();
  }



  insertImageDetails(imageDetails) {
    this.imageDetailList.push(imageDetails);
  }
  insertAMano(aMano) {
    this.imageAMano.push(aMano);
  }
  insertHardysAndSons(hardy) {
    this.imageHardysAndSons.push(hardy);
  }
  insertYellowTail(tail) {
    this.imageYellowTail.push(tail);
  }
  insertBarefoot(foot) {
    this.imageBarefoot.push(foot);
  }
  insertConchaToro(toro) {
    this.imageConchaToro.push(toro);
  }
  insertGallo(gallo) {
    this.imageGallo.push(gallo);
  }
  insertOthers(others) {
    this.imageOthers.push(others);
  }

  deleteWines($key: string) {
    this.imageYellowTail.remove($key);
    this.imageHardysAndSons.remove($key);
    this.imageYellowTail.remove($key);
    this.imageBarefoot.remove($key);
    this.imageConchaToro.remove($key);
    this.imageGallo.remove($key);
    this.imageOthers.remove($key);
  }

  onImageDelete(downloadUrl) {
    return this.imageDelete.storage.refFromURL(downloadUrl).delete();
  }

  updateYellow(tail) {
    this.imageYellowTail.update(tail.$key,
      {
        caption: tail.caption,
        category: tail.category,
        description: tail.description,
        allergen: tail.allergen,
        country: tail.country,
        region: tail.region,
        type: tail.type,
        alcohol: tail.alcohol
      });
  }

  updateHardy(hardy) {
    this.imageHardysAndSons.update(hardy.$key,
      {
        caption: hardy.caption,
        category: hardy.category,
        description: hardy.description,
        allergen: hardy.allergen,
        country: hardy.country,
        region: hardy.region,
        type: hardy.type,
        alcohol: hardy.alcohol
      });
  }

  updateAMano(aMano) {
    this.imageAMano.update(aMano.$key,
      {
        caption: aMano.caption,
        category: aMano.category,
        description: aMano.description,
        allergen: aMano.allergen,
        country: aMano.country,
        region: aMano.region,
        type: aMano.type,
        alcohol: aMano.alcohol
      });
  }

  updateBarefoot(foot) {
    this.imageBarefoot.update(foot.$key,
      {
        caption: foot.caption,
        category: foot.category,
        description: foot.description,
        allergen: foot.allergen,
        country: foot.country,
        region: foot.region,
        type: foot.type,
        alcohol: foot.alcohol
      });
  }

  updateConcha(toro) {
    this.imageConchaToro.update(toro.$key,
      {
        caption: toro.caption,
        category: toro.category,
        description: toro.description,
        allergen: toro.allergen,
        country: toro.country,
        region: toro.region,
        type: toro.type,
        alcohol: toro.alcohol
      });
  }

  updateGallo(gallo) {
    this.imageGallo.update(gallo.$key,
      {
        caption: gallo.caption,
        category: gallo.category,
        description: gallo.description,
        allergen: gallo.allergen,
        country: gallo.country,
        region: gallo.region,
        type: gallo.type,
        alcohol: gallo.alcohol
      });
  }

  updateOthers(others) {
    this.imageOthers.update(others.$key,
      {
        caption: others.caption,
        category: others.category,
        description: others.description,
        allergen: others.allergen,
        country: others.country,
        region: others.region,
        type: others.type,
        alcohol: others.alcohol
      });
  }
}
