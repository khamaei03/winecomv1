import { UploadService } from '../../../../shared/upload.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, ReactiveFormsModule  } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-wine-upload',
  templateUrl: './wine-upload.component.html',
  styleUrls: ['./wine-upload.component.scss']
})
export class WineUploadComponent implements OnInit {
  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;
  formTemplate: FormGroup;

  @Output() formSubmit = new EventEmitter<any>();
  @Output() wineFormSubmit = new EventEmitter<any>();

  constructor(
    private storage: AngularFireStorage,
    private service: UploadService,
    private fb: FormBuilder) {

    this.formTemplate = this.fb.group({
      caption: ['test', Validators.required],
      category: [''],
      hiddenImage: [''],
      imageUrl: ['', Validators.required],

      description: [''],
      allergen: [''],
      vintage: [''],
      country: [''],
      region: [''],
      type: [''],
      alcohol: [''],
    });
  }

  ngOnInit() {
    this.resetform();
  }

  resetform() {
    this.formTemplate.reset();
    this.formTemplate.setValue({
      caption: '',
      imageUrl: '',
      hiddenImage: '',
      category: '',

      description: [''],
      allergen: [''],
      vintage: [''],
      country: [''],
      region: [''],
      type: [''],
      alcohol: [''],
    });
    this.imgSrc = '/assets/img/cat_headphones.png';
    this.selectedImage = null;
    this.isSubmitted = false;
  }

  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.formTemplate.get('hiddenImage').patchValue(event.target.files[0]);
    }
    // tslint:disable-next-line: one-line
    else {
      this.imgSrc = '/assets/img/cat_headphones.png';
      this.formTemplate.get('hiddenImage').patchValue(this.imgSrc);
    }
  }

  onSubmit() {
    if (this.formTemplate.valid) {
      this.formSubmit.emit(this.formTemplate.value);
      this.wineFormSubmit.emit('hello world');
      this.resetform();
    } else {
      this.formTemplate.get('caption').markAsTouched();
    }
  }

  get formControls() {
    // tslint:disable-next-line: no-string-literal
    return this.formTemplate['controls'];
  }
}
