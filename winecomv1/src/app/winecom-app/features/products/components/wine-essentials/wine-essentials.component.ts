import { Component, OnInit } from '@angular/core';
import { UploadService } from 'src/app/winecom-app/shared/upload.service';

@Component({
  selector: 'app-wine-essentials',
  templateUrl: './wine-essentials.component.html',
  styleUrls: ['./wine-essentials.component.scss']
})
export class WineEssentialsComponent implements OnInit {

  constructor(private service: UploadService) { }

  ngOnInit() {
    this.service.getImageDetailList();
  }

}
