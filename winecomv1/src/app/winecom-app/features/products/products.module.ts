import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { WineUpdateDeleteComponent } from './components/wine-update-delete/wine-update-delete.component';
import { WineEssentialsComponent } from './components/wine-essentials/wine-essentials.component';
import { WineInfoComponent } from './components/wine-info/wine-info.component';
import { WineListGalleryComponent } from './components/wine-list-gallery/wine-list-gallery.component';
import { WineUpdateComponent } from './components/wine-update/wine-update.component';
import { WineUploadComponent } from './components/wine-upload/wine-upload.component';
import { WineUploadContainerComponent } from './container/wine-upload-container/wine-upload-container.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    WineUpdateDeleteComponent,
    WineEssentialsComponent,
    WineInfoComponent,
    WineListGalleryComponent,
    WineUpdateComponent,
    WineUploadComponent,
    WineUploadContainerComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [WineEssentialsComponent, WineUploadComponent, WineUpdateComponent, WineUpdateDeleteComponent, WineListGalleryComponent]
})
export class ProductsModule { }
