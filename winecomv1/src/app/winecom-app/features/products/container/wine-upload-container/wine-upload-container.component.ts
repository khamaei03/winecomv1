import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { AngularFireStorage } from '@angular/fire/storage';
import { UploadService } from '../../../../shared/upload.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-wine-upload-container',
  templateUrl: './wine-upload-container.component.html',
  styleUrls: ['./wine-upload-container.component.scss']
})
export class WineUploadContainerComponent implements OnInit {
  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;

  @Output() image: EventEmitter<any> = new EventEmitter<any>();

  constructor(private storage: AngularFireStorage, private service: UploadService) { }

  ngOnInit() {
  }

  onUploadSubmit(formValue) {
    // output
    // this.image.emit();
    console.log(formValue.hiddenImage.name);
    const filePath = `${formValue.category}/${formValue.hiddenImage.name.split('.').slice(0, -1).join('.')}_${new Date().getTime()}`;
    const fileRef = this.storage.ref(filePath);
    this.storage.upload(filePath, formValue.hiddenImage).snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe((url) => {
          // tslint:disable-next-line: no-string-literal
          formValue['imageUrl'] = url;
          if (formValue.category === 'A Mano') { this.service.insertAMano(formValue); }
          if (formValue.category === 'Hardys and Sons') { this.service.insertHardysAndSons(formValue); }
          if (formValue.category === 'YellowTail') { this.service.insertYellowTail(formValue); }
          if (formValue.category === 'Barefoot') { this.service.insertBarefoot(formValue); }
          if (formValue.category === 'Concha Toro') { this.service.insertConchaToro(formValue); }
          if (formValue.category === 'Others') { this.service.insertOthers(formValue); }
        });
      })
    ).subscribe();
  }

}
