import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WineInfoComponent } from './components/wine-info/wine-info.component';

const routes: Routes = [
 {
     path: '',
     component: WineInfoComponent
 }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
