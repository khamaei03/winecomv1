import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../users/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
    // child component


  constructor(
    private router: Router) { }
    year: number = 2019;
    count: number = 0;

  //contents inside
  ngOnInit() {
    if(!localStorage.getItem('hasLoadedPrompt')) 
    {
      localStorage.setItem('hasLoadedPrompt', '1');
      this.router.navigate(['/dashboard/prompt']);
    }
  }

  setClasses() {
    let myClasses = {
      over21: this.count == 1
    }
    return myClasses;
  }

  onClick() {
    this.count = 1;
  }



  
}
