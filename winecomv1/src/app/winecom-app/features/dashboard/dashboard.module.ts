import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PromptMessageComponent } from './prompt-message/prompt-message.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { UsersModule } from '../users/users.module';
import { WineryPageComponent } from './winery-page/winery-page.component';
import { ProductsModule } from '../products/products.module';


@NgModule({
  declarations: [
    NavigationBarComponent,
    HomepageComponent,
    PromptMessageComponent,
    AboutUsComponent,
    WineryPageComponent,
  
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    UsersModule,
    ProductsModule
  ]
})
export class DashboardModule { 
}
