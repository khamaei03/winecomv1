import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { PromptMessageComponent} from './prompt-message/prompt-message.component';
import { LoginComponent } from '../users/components/login/login.component';
import { WineryPageComponent } from './winery-page/winery-page.component';



const routes: Routes = [
    { path: '', component: PromptMessageComponent },
    { path: 'about-us', component: AboutUsComponent },
    { path: 'prompt', component: PromptMessageComponent },
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomepageComponent },
    { path: 'winery', component: WineryPageComponent },
    { path: 'gallery', component: AboutUsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
