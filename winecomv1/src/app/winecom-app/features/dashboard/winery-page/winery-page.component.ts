import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-winery-page',
  templateUrl: './winery-page.component.html',
  styleUrls: ['./winery-page.component.scss']
})
export class WineryPageComponent implements OnInit {

  wineryChoice: string;
  constructor(private route: ActivatedRoute) {
    this.route.fragment.subscribe(value => {
      this.wineryChoice = value;
      console.log(value, 'wineryChoice');
  });
  }

  ngOnInit() {
    //this.getValues()
  }

  getValues(wineryDropDownChoice) {
    console.log('test')
    this.wineryChoice = wineryDropDownChoice;
    console.log(this.wineryChoice);
  }

}
