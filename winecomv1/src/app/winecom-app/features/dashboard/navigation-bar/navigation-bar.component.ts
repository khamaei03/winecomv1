import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../users/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {

  // parent component

  // child component to winery
  @Output() wineryNavChoice: EventEmitter<any> = new EventEmitter<any>();

  user: firebase.User;
  constructor(
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.auth.getUserState()
    .subscribe(user => {
     this.user = user;
     console.log(this.user);
    })
  }

  login() {
    this.router.navigate(['/login'])
  }
 
  logout() {
    this.auth.logout();
    this.router.navigate(['/login'])
  }

  register() {
    this.router.navigate(['/registration'])
  }

  wineryPageChoice(a:string) {
    this.router.navigate(['/winery'], { fragment: a })
  }

}
