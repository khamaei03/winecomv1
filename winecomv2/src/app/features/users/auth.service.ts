import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

import { BehaviorSubject, Observable } from 'rxjs';
import { User } from './../../shared/models/user.model';
import { Register } from './../../shared/models/registration.model';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: Observable<firebase.User>;
  private regis: Observable<Register>;
  private eventAuthError = new BehaviorSubject<string>('');
  eventAuthError$ = this.eventAuthError.asObservable();

  newUser: Register = new Register();

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router) {
    this.user = afAuth.authState;
    this.regis = afAuth.authState;
  }


  // loginextra(user: User) {
  //   return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);

  // }

  login(user) {
    console.log(user, 'login');
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
      .catch(error => {
        // this.eventAuthError.next(error);
      })
      .then(userCredential => {
        if (userCredential) {
          this.router.navigate(['/home']);
        }
      });
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  authUser() {
    return this.user;
  }

  getUserState() {
    return this.afAuth.authState;
  }

  createUserRegistration(user) {
    this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(userCredential => {
        this.newUser = user;
        console.log(userCredential);
        userCredential.user.updateProfile({
          displayName: user.firstName + ' ' + user.lastName
        });

        this.insertUserData(userCredential)
          .then(() => {
            this.router.navigate(['/login']);
          });
      })
      .catch(error => {
        // this.eventAuthError.next(error);
      });
  }

  // doc = ref, set = put for no error
  insertUserData(userCredential: firebase.auth.UserCredential) {
    return this.db.doc(`Users/${userCredential.user.uid}`).set({
      email: this.newUser.email,
      firstname: this.newUser.firstName,
      lastname: this.newUser.lastName,
      role: 'network user'
    });
  }
}
