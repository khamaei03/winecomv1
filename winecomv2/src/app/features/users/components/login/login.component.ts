import { Component, OnInit, } from '@angular/core';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, EmailValidator } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorMsg: string;
  authError: any;
  snackbarMessage: string;
  isSubmitted = true;
  isPassword = true;
  isEmail = true;

  formLogin: FormGroup;
  constructor(
    private auth: AuthService,
    private router: Router,
    private fl: FormBuilder) { }

  ngOnInit() {

    this.formLogin = this.fl.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  // loginWine() {
  //   this.auth.loginextra({ email: this.formLogin.controls.email.value, password: this.formLogin.controls.password.value })
  //     .then(resolve => this.router.navigate(['home']))
  //     .catch(error => this.errorMsg = error.message);
  // }

  login(form) {
    console.log(form, 'login');
    this.auth.login(form);
  }

  loginValidators() {
    if (this.formLogin.value.email === '') { this.isEmail = false; }
    if (this.formLogin.value.password === '') { this.isPassword = false; }
  }

  loginDirty() {
    if (this.formLogin.touched) { this.isEmail = true; }
    if (this.formLogin.touched) { this.isPassword = true; }
  }

  register() {
    this.router.navigate(['registration']);

  }
}
