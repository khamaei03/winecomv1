import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { PasswordValidator } from 'src/app/shared/email.validators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})

export class RegistrationComponent implements OnInit {

  newUser: any;
  authError: any;
  snackbarMessage: string = "redirected to registration";
  constructor(
    private auth: AuthService,
    private fr: FormBuilder,
    private router: Router) { }

  private eventAuthError = new BehaviorSubject<string>('');
  eventAuthError$ = this.eventAuthError.asObservable();

  isSubmitted = true;
  formRegistration: FormGroup;
  ngOnInit() {
    this.auth.eventAuthError$.subscribe(data => {
      this.authError = data;
    });

    this.formRegistration = this.fr.group({

      firstName: ['', Validators.required],
      lastName: [''],
      email: ['', Validators.required],
      password: [''],
      confirmPassword: ['']
    },
      {
        validator: PasswordValidator
      });
  }

  createUser(frm) {
    this.isSubmitted = false;
    this.auth.createUserRegistration(frm);
  }

  get formControls() {
    // tslint:disable-next-line: no-string-literal
    return this.formRegistration['controls'];
  }

  backToLogin() {
    this.router.navigate(['login']);
  }
}
