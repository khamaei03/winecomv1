import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-prompt-message',
  templateUrl: './prompt-message.component.html',
  styleUrls: ['./prompt-message.component.scss']
})
export class PromptMessageComponent implements OnInit {

  constructor(private router: Router) { }

  // tslint:disable-next-line: no-inferrable-types
  count: number = 0;

  ngOnInit() {
  }

  setClasses() {
    const myClasses = {
      over21: this.count === 1
    };
    return myClasses;
  }

  // onClick() {
  //   this.count = 1;
  // }

  login() {
    this.router.navigate(['/users/login']);
  }
}
