import { Component, OnInit } from '@angular/core';
// import { ImageService } from '../../../../app/shared/image.service';
import { ImageService } from './../../../shared/image.service';

@Component({
  selector: 'app-gallery-page',
  templateUrl: './gallery-page.component.html',
  styleUrls: ['./gallery-page.component.scss']
})
export class GalleryPageComponent implements OnInit {

  imageList: any[];
  amanoList: any[];
  hardyList: any[];
  yellowList: any[];
  barefootList: any[];
  conchaList: any[];
  galloList: any[];
  othersList: any[];

  rowIndexArray: any[];
  rowHardyArray: any[];
  rowYellowArray: any[];
  rowBarefootArray: any[];
  rowConchaArray: any[];
  rowGalloArray: any[];
  rowOthersArray: any[];


  constructor(private service: ImageService) { }

  ngOnInit() {
    this.service.getImageDetailList();
    // AMANO DISPLAY
    this.service.imageAMano.snapshotChanges().subscribe(
      list => {
        this.amanoList = list.map(item => {
          return item.payload.val();
        });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.amanoList.length / 5)).keys());
      }
    );

    // HARDY AND SONS DISPLAY
    this.service.imageHardysAndSons.snapshotChanges().subscribe(
      list => {
        this.hardyList = list.map(item => {
          return item.payload.val();
        });
        this.rowHardyArray = Array.from(Array(Math.ceil(this.hardyList.length / 5)).keys());
      }
    );

    // YELLOWTAIL DISPLAY
    this.service.imageYellowTail.snapshotChanges().subscribe(
      list => {
        this.yellowList = list.map(item => {
          return item.payload.val();
        });
        this.rowYellowArray = Array.from(Array(Math.ceil(this.yellowList.length / 5)).keys());
      }
    );

    // BAREFOOT DISPLAY
    this.service.imageBarefoot.snapshotChanges().subscribe(
      list => {
        this.barefootList = list.map(item => {
          return item.payload.val();
        });
        this.rowBarefootArray = Array.from(Array(Math.ceil(this.barefootList.length / 5)).keys());
      }
    );

    // CONCHA TORO DISPLAY
    this.service.imageConchaToro.snapshotChanges().subscribe(
      list => {
        this.conchaList = list.map(item => {
          return item.payload.val();
        });
        this.rowConchaArray = Array.from(Array(Math.ceil(this.conchaList.length / 5)).keys());
      }
    );

    // GALLO DISPLAY
    this.service.imageGallo.snapshotChanges().subscribe(
      list => {
        this.galloList = list.map(item => {
          return item.payload.val();
        });
        this.rowGalloArray = Array.from(Array(Math.ceil(this.galloList.length / 5)).keys());
      }
    );

    // OTHERS DISPLAY
    this.service.imageOthers.snapshotChanges().subscribe(
      list => {
        this.othersList = list.map(item => {
          return item.payload.val();
        });
        this.rowOthersArray = Array.from(Array(Math.ceil(this.othersList.length / 5)).keys());
      }
    );
  }
}

