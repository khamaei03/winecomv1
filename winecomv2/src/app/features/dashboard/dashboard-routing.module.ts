import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Importing Dashboard components
import { HomepageComponent } from './homepage/homepage.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { PromptMessageComponent } from './prompt-message/prompt-message.component';
import { LoginComponent } from '../users/components/login/login.component';
import { WineryPageComponent } from './winery-page/winery-page.component';
import { GalleryPageComponent } from './gallery-page/gallery-page.component';

import { AuthenticationGuard } from 'src/app/shared/authenticationGuard.service';

import { ContactUsComponent } from './contact-us/contact-us.component';
import { LocationComponent } from './location/location.component';
import { SingleProductComponent } from '../products/component/single-product/single-product.component';

const routes: Routes = [
    // temporary redirect routing
    // { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '', component: PromptMessageComponent},
    { path: 'about-us', component: AboutUsComponent, canActivate: [AuthenticationGuard]},
    { path: 'prompt', component: PromptMessageComponent, canActivate: [AuthenticationGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomepageComponent, canActivate: [AuthenticationGuard] },
    { path: 'winery', component: WineryPageComponent, canActivate: [AuthenticationGuard] },
    { path: 'gallery', component: GalleryPageComponent, canActivate: [AuthenticationGuard]},
    { path: 'location', component: LocationComponent, canActivate: [AuthenticationGuard]},
    { path: 'contact', component: ContactUsComponent, canActivate: [AuthenticationGuard]},
    { path: 'single-product', component: SingleProductComponent, canActivate: [AuthenticationGuard]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
