import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-winery-page',
  templateUrl: './winery-page.component.html',
  styleUrls: ['./winery-page.component.scss']
})
export class WineryPageComponent implements OnInit {
  // child component
  @Output() brandsChoice: EventEmitter<any> = new EventEmitter<any>();

  snackbarMessage: string;
  wineryChoice: string;
  constructor(private route: ActivatedRoute, private router: Router ) {
    this.route.fragment.subscribe(value => {
      this.wineryChoice = value;
      console.log(value, 'wineryChoice');
    });
  }

  ngOnInit() {
    // this.getValues()
    
  }

  // getValues(wineryDropDownChoice) {
  //   console.log('test');
  //   this.wineryChoice = wineryDropDownChoice;
  //   console.log(this.wineryChoice);
  // }


onClickCategory(a: string) {
  this.wineryChoice = a;
  console.log(this.wineryChoice);
}

  wineryPageChoice(a) {
    this.router.navigate(['/winery'], { fragment: a });
  
  }
}
