import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../users/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

    // child component
    // tslint:disable

  user: firebase.User;
  constructor(
    private auth: AuthService,
    private router: Router) { }
    year: number = 2019;
    count: number = 0;
    snackBarMessage: string;

  // contents inside
  ngOnInit() {
    
    if (!localStorage.getItem('hasLoadedPrompt')) {
      localStorage.setItem('hasLoadedPrompt', '1');
      this.router.navigate(['/dashboard/prompt']);
    }
    this.auth.getUserState()
    .subscribe(user => {
     this.user = user;
     console.log(this.user);
    });
    // snackbar snippet
    setTimeout(function(){const x = document.getElementById('snackbar');
    x.className = 'show';
    setTimeout(function() { x.className = x.className.replace('show', ''); }, 3000);
    this.snackBarMessage = 'Welcome to Winecom!';},1000);
    
  }

  setClasses() {
    const myClasses = {
      over21: this.count === 1
    };
    return myClasses;
  }

  onClick() {
    this.count = 1;
  }
}
