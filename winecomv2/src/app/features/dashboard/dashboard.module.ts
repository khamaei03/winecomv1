import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// To import dashboard components
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { HomepageComponent } from './homepage/homepage.component';
import { PromptMessageComponent } from './prompt-message/prompt-message.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { WineryPageComponent } from './winery-page/winery-page.component';
import { NavLinksComponent } from './navigation-bar/nav-links/nav-links.component';
import { GalleryPageComponent } from './gallery-page/gallery-page.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { ImageGalleryComponent } from './gallery-page/image-gallery/image-gallery.component';

// To import other modules
import { UsersModule } from '../users/users.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ProductsModule } from '../products/products.module';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LocationComponent } from './location/location.component';
import { FooterComponent } from './footer/footer.component';




@NgModule({
  declarations: [
    NavigationBarComponent,
    HomepageComponent,
    PromptMessageComponent,
    AboutUsComponent,
    WineryPageComponent,
    NavLinksComponent,
    GalleryPageComponent,
    ImageGalleryComponent,
    ErrorpageComponent,
    ContactUsComponent,
    LocationComponent, 
    FooterComponent

  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    UsersModule,
    ProductsModule,
    
  ],
  exports: [
    NavigationBarComponent,
    NavLinksComponent

  ]

})
export class DashboardModule {
}
