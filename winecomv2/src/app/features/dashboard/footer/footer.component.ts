import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  wineryChoice: string;
  constructor(private route: ActivatedRoute, private router: Router) {  this.route.fragment.subscribe(value => {
    this.wineryChoice = value;
    console.log(value, 'wineryChoice');
  });}

  ngOnInit() {
  }

  wineryPageChoice() {
    this.router.navigate(['/winery'], { fragment: 'brands' });
  }
}
