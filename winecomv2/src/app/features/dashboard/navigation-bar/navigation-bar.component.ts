import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../users/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {

  // parent component
  // child component to winery

  @Output() wineryNavChoice: EventEmitter<any> = new EventEmitter<any>();
  @Output() goToGalleryPage: EventEmitter<any> = new EventEmitter<any>();
  @Output() goToContactPage: EventEmitter<any> = new EventEmitter<any>();
  @Output() goToLocationPage: EventEmitter<any> = new EventEmitter<any>();

  user: firebase.User;
  constructor(
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.auth.getUserState()
    .subscribe(user => {
     this.user = user;
     console.log(this.user);
    });
  }

  login() {
    this.router.navigate(['/login']);
    window.scrollTo(0, 0);
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/login']);
    window.scrollTo(0, 0);
  }

  register() {
    this.router.navigate(['/registration']);
    window.scrollTo(0, 0);
  }

  wineryPageChoice(a) {
    this.router.navigate(['/winery'], { fragment: a });
    window.scrollTo(0, 0);
  }

  onGalleryClick() {
    this.goToGalleryPage.emit('gallerySelected');
    console.log('galleryTest');
    window.scrollTo(0, 0);
  }

  onContactClick() {
    this.goToContactPage.emit('contactSelected');
    console.log('contactTest');
    window.scrollTo(0, 0);
  }

  onLocationClick() {
    this.goToLocationPage.emit('locationSelected');
    console.log('locationTest');
    window.scrollTo(0, 0);
  }
}
