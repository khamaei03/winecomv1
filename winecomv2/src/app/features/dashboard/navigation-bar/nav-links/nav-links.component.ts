import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/features/users/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-links',
  templateUrl: './nav-links.component.html',
  styleUrls: ['./nav-links.component.scss']
})
export class NavLinksComponent implements OnInit {

  user: firebase.User;
  constructor(
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.auth.getUserState()
    .subscribe(user => {
     this.user = user;
     console.log(this.user);
    });

  }
  login() {
    this.router.navigate(['/login']);
    window.scrollTo(0, 0);
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/login']);
    window.scrollTo(0, 0);
  }

  goToHome() {
    this.router.navigate(['/home']);
    window.scrollTo(0, 0);
  }

}
