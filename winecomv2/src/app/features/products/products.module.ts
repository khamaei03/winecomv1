import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// importing Product Components
import { ImageComponent } from '../products/component/image/image.component';
import { ImagesComponent } from './component/images/images.component';
import { ImageDetailsComponent } from './component/image-details/image-details.component';
import { ImageListComponent } from './component/image-list/image-list.component';
import { UploadComponent } from './container/upload/upload.component';
import { BrandsComponent } from './component/brands/brands.component';

import { ProductRoutingModule } from './product-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { DashboardModule } from '../dashboard/dashboard.module';
import { SingleProductComponent } from './component/single-product/single-product.component';



@NgModule({
  declarations: [
    ImageComponent,
    ImagesComponent,
    ImageDetailsComponent,
    ImageListComponent,
    UploadComponent,
    BrandsComponent,
    SingleProductComponent,

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    ProductRoutingModule,
    NgxPaginationModule,
  ],

  exports: [
    ImagesComponent,
    ImageListComponent,
    ImageDetailsComponent,
    ImageComponent,
    UploadComponent,
    BrandsComponent
  ]

})
export class ProductsModule { }
