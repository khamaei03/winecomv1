import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadComponent } from './container/upload/upload.component';
import { ImageListComponent } from './component/image-list/image-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'upload', pathMatch: 'full'},
  { path: 'upload', component: UploadComponent},
  { path: 'list', component: ImageListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
