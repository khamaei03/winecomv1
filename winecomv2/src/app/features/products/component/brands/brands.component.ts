import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss']
})
export class BrandsComponent implements OnInit {

  // child component

  @Output() brandClick = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onClick(a) {
    console.log(a);
    this.brandClick.emit(a);
  }

}
