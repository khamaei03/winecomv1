import { ImageService } from '../../../../shared/image.service';
import { Component, OnInit, Input } from '@angular/core';
import { ImageComponent } from '../image/image.component';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
// import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-image-details',
  templateUrl: './image-details.component.html',
  styleUrls: ['./image-details.component.scss']
})

export class ImageDetailsComponent implements OnInit {
  formTemplate: FormGroup;
  imageWidth = 50;
  imageMargin = 2;
  snackbarMessage: string;

  // @ViewChild('yellowTailModal') public modal: ModalDirective;
  constructor(
    private storage: AngularFireStorage,
    private service: ImageService,
    private fb: FormBuilder) {

      this.formTemplate = this.fb.group({
        $key: [null],
        caption: ['', Validators.required],
        category: ['', Validators.required],
        imageUrl: ['', Validators.required],
        description: ['', Validators.required],
        allergen: ['', Validators.required],
        vintage: ['', Validators.required],
        country: ['', Validators.required],
        region: ['', Validators.required],
        type: ['', Validators.required],
        alcohol: ['', Validators.required],
      });
    }

  @Input() public form: string;

  winesYellowTail: any[];
  winesHardys: any[];
  winesAMano: any[];
  winesBarefoot: any[];
  winesConchaToro: any[];
  winesGallo: any[];
  winesOthers: any[];

  filter: string;

  onClickFilter(valueFromHTML: string) {
    return this.filter = valueFromHTML;
  }

  ngOnInit() {

    this.filter = 'A Mano';
    // YellowTail
    this.service.getImageDetailList().subscribe(
      list => {
        this.winesYellowTail = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Hardys And Sons
    this.service.getImageHardys().subscribe(
      list => {
        this.winesHardys = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // AMano
    this.service.getImageAMano().subscribe(
      list => {
        this.winesAMano = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Barefoot
    this.service.getImageBarefoot().subscribe(
      list => {
        this.winesBarefoot = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Concha Toro
    this.service.getImageConchaToro().subscribe(
      list => {
        this.winesConchaToro = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Gallo
    this.service.getImageGallo().subscribe(
      list => {
        this.winesGallo = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Others
    this.service.getImageOthers().subscribe(
      list => {
        this.winesOthers = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
  }

  onDelete($key, downloadUrl) {
    if (confirm('Are you sure to delete this record ?')) {
      this.service.deleteWines($key);
      this.service.onImageDelete(downloadUrl);
    }
  }

  populateForm(wines) {
    this.formTemplate.setValue(wines);
  }


  // caption: ['', Validators.required],
  //       category: ['', Validators.required],
  //       imageUrl: ['', Validators.required],
  //       description: ['', Validators.required],
  //       allergen: ['', Validators.required],
  //       vintage: ['', Validators.required],
  //       country: ['', Validators.required],
  //       region: ['', Validators.required],
  //       type: ['', Validators.required],
  //       alcohol: ['', Validators.required],
  onSubmit() {
    if (this.formTemplate.value.caption === '' ||
    this.formTemplate.value.category === '' ||
    this.formTemplate.value.hiddenImage === '' ||
    this.formTemplate.value.imageUrl === '' ||
    this.formTemplate.value.description === '' ||
    this.formTemplate.value.allergen === '' ||
    this.formTemplate.value.country === '' ||
    this.formTemplate.value.type === '' ||
    this.formTemplate.value.alcohol === '%'
   ) {
    const x = document.getElementById('snackbar');
    this.snackbarMessage = 'Please complete all fields!';
    x.className = 'error';
    setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
    console.log('snackbar test');
    // this.
   } else {
    this.service.updateYellow(this.formTemplate.value);
    document.getElementById('yellowTailModal').click();
    }
  }
  onSubmitHardy() {
    if (this.formTemplate.value.caption === '' ||
    this.formTemplate.value.category === '' ||
    this.formTemplate.value.hiddenImage === '' ||
    this.formTemplate.value.imageUrl === '' ||
    this.formTemplate.value.description === '' ||
    this.formTemplate.value.allergen === '' ||
    this.formTemplate.value.country === '' ||
    this.formTemplate.value.type === '' ||
    this.formTemplate.value.alcohol === '%'
   ) {
    const x = document.getElementById('snackbar');
    this.snackbarMessage = 'Please complete all fields!';
    x.className = 'error';
    setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
    console.log('snackbar test');
    // this.
   } else {
    this.service.updateHardy(this.formTemplate.value);
    document.getElementById('hardyModal').click();
    }
  }
  onSubmitAMano() {
    if (this.formTemplate.value.caption === '' ||
    this.formTemplate.value.category === '' ||
    this.formTemplate.value.hiddenImage === '' ||
    this.formTemplate.value.imageUrl === '' ||
    this.formTemplate.value.description === '' ||
    this.formTemplate.value.allergen === '' ||
    this.formTemplate.value.country === '' ||
    this.formTemplate.value.type === '' ||
    this.formTemplate.value.alcohol === '%'
   ) {
    const x = document.getElementById('snackbar');
    this.snackbarMessage = 'Please complete all fields!';
    x.className = 'error';
    setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
    console.log('snackbar test');
    // this.
   } else {
    this.service.updateAMano(this.formTemplate.value);
    document.getElementById('AManoModal').click();
    }
  }
  onSubmitBarefoot() {
    if (this.formTemplate.value.caption === '' ||
    this.formTemplate.value.category === '' ||
    this.formTemplate.value.hiddenImage === '' ||
    this.formTemplate.value.imageUrl === '' ||
    this.formTemplate.value.description === '' ||
    this.formTemplate.value.allergen === '' ||
    this.formTemplate.value.country === '' ||
    this.formTemplate.value.type === '' ||
    this.formTemplate.value.alcohol === '%'
   ) {
    const x = document.getElementById('snackbar');
    this.snackbarMessage = 'Please complete all fields!';
    x.className = 'error';
    setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
    console.log('snackbar test');
    // this.
   } else {
    this.service.updateBarefoot(this.formTemplate.value);
    document.getElementById('BarefootModal').click();
    }
  }
  onSubmitConcha() {
    if (this.formTemplate.value.caption === '' ||
    this.formTemplate.value.category === '' ||
    this.formTemplate.value.hiddenImage === '' ||
    this.formTemplate.value.imageUrl === '' ||
    this.formTemplate.value.description === '' ||
    this.formTemplate.value.allergen === '' ||
    this.formTemplate.value.country === '' ||
    this.formTemplate.value.type === '' ||
    this.formTemplate.value.alcohol === '%'
   ) {
    const x = document.getElementById('snackbar');
    this.snackbarMessage = 'Please complete all fields!';
    x.className = 'error';
    setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
    console.log('snackbar test');
    // this.
   } else {
    this.service.updateConcha(this.formTemplate.value);
    document.getElementById('ConchaModal').click();
    }
  }
  onSubmitOthers() {
    if (this.formTemplate.value.caption === '' ||
    this.formTemplate.value.category === '' ||
    this.formTemplate.value.hiddenImage === '' ||
    this.formTemplate.value.imageUrl === '' ||
    this.formTemplate.value.description === '' ||
    this.formTemplate.value.allergen === '' ||
    this.formTemplate.value.country === '' ||
    this.formTemplate.value.type === '' ||
    this.formTemplate.value.alcohol === '%'
   ) {
    const x = document.getElementById('snackbar');
    this.snackbarMessage = 'Please complete all fields!';
    x.className = 'error';
    setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
    console.log('snackbar test');
    // this.
   } else {
    this.service.updateOthers(this.formTemplate.value);
    document.getElementById('OthersModal').click();
    }
  }
  // onSubmitHardy() {
  //   this.service.updateHardy(this.formTemplate.value);
  // }
  // onSubmitAMano() {
  //   this.service.updateAMano(this.formTemplate.value);
  // }
  // onSubmitBarefoot() {
  //   this.service.updateBarefoot(this.formTemplate.value);
  // }
  // onSubmitConcha() {
  //   this.service.updateConcha(this.formTemplate.value);
  // }
  // onSubmitGallo() {
  //   this.service.updateGallo(this.formTemplate.value);
  // }
  // onSubmitOthers() {
  //   this.service.updateOthers(this.formTemplate.value);
  // }
}
