import { Component, OnInit } from '@angular/core';
import { ImageService } from '../../../../shared/image.service';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})

export class ImageListComponent implements OnInit {

  // child component of Gallery Page Component
  onGalleryPageClickValue: string;
  chosenBrand: string;
  imageClicked = true;
  imageClicked2 = true;
  imageClicked3 = true;

  imageList: any[];
  amanoList: any[];
  hardyList: any[];
  yellowList: any[];
  barefootList: any[];
  conchaList: any[];
  galloList: any[];
  othersList: any[];

  rowIndexArray: any[];
  rowHardyArray: any[];
  rowYellowArray: any[];
  rowBarefootArray: any[];
  rowConchaArray: any[];
  rowGalloArray: any[];
  rowOthersArray: any[];

  anyDetails: any[];

  constructor(private service: ImageService) { }

  ngOnInit() {
    // AMANO DISPLAY
    this.service.imageAMano.snapshotChanges().subscribe(
      list => {
        this.amanoList = list.map(item => {
          return item.payload.val();
        });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.amanoList.length / 5)).keys());
      }
    );

    // HARDY AND SONS DISPLAY
    this.service.imageHardysAndSons.snapshotChanges().subscribe(
      list => {
        this.hardyList = list.map(item => {
          return item.payload.val();
        });
        this.rowHardyArray = Array.from(Array(Math.ceil(this.hardyList.length / 5)).keys());
      }
    );

    // YELLOWTAIL DISPLAY
    this.service.imageYellowTail.snapshotChanges().subscribe(
      list => {
        this.yellowList = list.map(item => {
          return item.payload.val();
        });
        this.rowYellowArray = Array.from(Array(Math.ceil(this.yellowList.length / 5)).keys());
      }
    );

    // BAREFOOT DISPLAY
    this.service.imageBarefoot.snapshotChanges().subscribe(
      list => {
        this.barefootList = list.map(item => {
          return item.payload.val();
        });
        this.rowBarefootArray = Array.from(Array(Math.ceil(this.barefootList.length / 5)).keys());
      }
    );

    // CONCHA TORO DISPLAY
    this.service.imageConchaToro.snapshotChanges().subscribe(
      list => {
        this.conchaList = list.map(item => {
          return item.payload.val();
        });
        this.rowConchaArray = Array.from(Array(Math.ceil(this.conchaList.length / 5)).keys());
      }
    );

    // GALLO DISPLAY
    this.service.imageGallo.snapshotChanges().subscribe(
      list => {
        this.galloList = list.map(item => {
          return item.payload.val();
        });
        this.rowGalloArray = Array.from(Array(Math.ceil(this.galloList.length / 5)).keys());
      }
    );

    // OTHERS DISPLAY
    this.service.imageOthers.snapshotChanges().subscribe(
      list => {
        this.othersList = list.map(item => {
          return item.payload.val();
        });
        this.rowOthersArray = Array.from(Array(Math.ceil(this.othersList.length / 5)).keys());
      }
    );
  }

  // IMAGE RETRIEVAL ARRAY
  getData(get) {
    this.anyDetails = get;
  }

  receiveBrandName(a: string) {
    this.chosenBrand = a;
    console.log(this.chosenBrand + 'this is received from Parent Component');
  }

  clickFalse1() {
    this.imageClicked = false;
    this.imageClicked2 = true;
    this.imageClicked3 = true;
  }

  clickFalse2() {
    this.imageClicked = true;
    this.imageClicked2 = false;
    this.imageClicked3 = true;
  }

  clickFalse3() {
    this.imageClicked = true;
    this.imageClicked2 = true;
    this.imageClicked3 = false;
  }


  getGalleryPageValue(a: string) {
    this.onGalleryPageClickValue = a;
    console.log('from gallery Page');
  }

  onClick(a) {
    console.log(a);
  }
}
