import { ImageService } from '../../../../shared/image.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {

  // tslint:disable

  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;
  formTemplate: FormGroup;
  winesList: any[];
  snackbarMessage: string;

  defaultPic: boolean;
  saveBlank: boolean;

  public formTransfer = this.formTemplate;
  public givenForm = this.formTemplate;

  @Output() formSubmit = new EventEmitter<any>();
  @Output() wineFormSubmit = new EventEmitter<any>();

  ImageService: any;

  constructor(
    private storage: AngularFireStorage,
    private service: ImageService,
    private fb: FormBuilder,
    private router: Router) {

    this.formTemplate = this.fb.group({

      caption: ['', Validators.required],
      category: ['', Validators.required],
      hiddenImage: [''],
      imageUrl: ['', Validators.required],
      description: ['', Validators.required],
      allergen: ['', Validators.required],
      vintage: ['', Validators.required],
      country: ['', Validators.required],
      region: ['', Validators.required],
      type: ['', Validators.required],
      alcohol: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.resetform();
    this.defaultPic = true;
    }

  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.formTemplate.get('hiddenImage').patchValue(event.target.files[0]);
      this.defaultPic = false;
    } else {
      this.imgSrc = '/assets/images/transpwine.png';
      this.formTemplate.get('hiddenImage').patchValue(this.imgSrc);
      this.defaultPic = true;
    }
  }

  onSubmit() {
    if (this.formTemplate.value.caption === '' ||
        this.formTemplate.value.category === '' ||
        this.formTemplate.value.hiddenImage === '' ||
        this.formTemplate.value.imageUrl === '' ||
        this.formTemplate.value.description === '' ||
        this.formTemplate.value.allergen === '' ||
        this.formTemplate.value.country === '' ||
        this.formTemplate.value.type === '' ||
        this.formTemplate.value.alcohol === ''
       ) {
        this.formTemplate.controls.caption.markAsTouched();
        this.formTemplate.controls.category.markAsTouched();
        this.formTemplate.controls.hiddenImage.markAsTouched();
        this.formTemplate.controls.imageUrl.markAsTouched();
        this.formTemplate.controls.description .markAsTouched();
        this.formTemplate.controls.allergen.markAsTouched();
        this.formTemplate.controls.country.markAsTouched();
        this.formTemplate.controls.type.markAsTouched();
        this.formTemplate.controls.alcohol.markAsTouched();

        const x = document.getElementById('snackbar');
        this.snackbarMessage = 'Please complete all fields!';
        x.className = 'error';
        setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
        } else {
      this.formSubmit.emit(this.formTemplate.value);
      this.wineFormSubmit.emit('hello world');
      this.resetform();
      this.defaultPic = true;



      // snackbar message kembas
      const x = document.getElementById('snackbar');
      this.snackbarMessage = 'Wine Item Uploaded!';
      x.className = 'show';
      setTimeout(function() { x.className = x.className.replace('show', ''); }, 3000);
      }

  }

  // submitEmpty() {
  //   if (
  //   this.formTemplate.value.caption === '' ||
  //   this.formTemplate.value.category === '' ||
  //   this.formTemplate.value.hiddenImage === '' ||
  //   this.formTemplate.value.imageUrl === '' ||
  //   this.formTemplate.value.description === '' ||
  //   this.formTemplate.value.allergen === '' ||
  //   this.formTemplate.value.country === '' ||
  //   this.formTemplate.value.type === '' ||
  //   this.formTemplate.value.alcohol === ''
  //  ) {
    

  //   const x = document.getElementById('snackbar');
  //   this.snackbarMessage = 'Please complete all fields!';
  //   x.className = 'error';
  //   setTimeout(function() { x.className = x.className.replace('error', ''); }, 4000);
  //  }
  // }

  get formControls() {
    // tslint:disable-next-line: no-string-literal
    return this.formTemplate['controls'];
  }

  toProductList() {
    this.router.navigate(['/winery'], { fragment: 'update' });
  }

  resetform() {
    this.formTemplate.reset();
    this.formTemplate.setValue({
      caption: '',
      imageUrl: '',
      hiddenImage: '',
      category: '',
      description: '',
      allergen: '',
      vintage: '',
      country: '',
      region: '',
      type: '',
      alcohol: ''
    });
    this.imgSrc = '/assets/images/transpwine.png';
    this.selectedImage = null;
    this.isSubmitted = false;
  }

  // loginValidators() {
  //   if (this.formLogin.value.email === '') { this.isEmail = false; }
  //   if (this.formLogin.value.password === '') { this.isPassword = false; }
  // }

  // loginDirty() {
  //   if (this.formLogin.touched) { this.isEmail = true; }
  //   if (this.formLogin.touched) { this.isPassword = true; }
  // }

}
