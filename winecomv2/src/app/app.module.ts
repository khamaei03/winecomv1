// Original Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Database Modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

// Component Modules
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardModule } from './features/dashboard/dashboard.module';
import { UsersModule } from './features/users/users.module';
import { ProductsModule } from './features/products/products.module';



// Services
import { AuthenticationGuard } from './shared/authenticationGuard.service';
import { AuthService } from 'src/app/features/users/auth.service';
import { ImageService } from '././shared/image.service';

// Peripherals
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, environment.firebaseConfig2),
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,

    DashboardModule,
    UsersModule,
    ProductsModule,
    FormsModule,
    ReactiveFormsModule,

    BrowserAnimationsModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      toastClass: 'toast toast-bootstrap-compatibility-fix'
    }),

  ],
  providers: [
    ImageService,
    AuthService,
    AuthenticationGuard],
  bootstrap: [AppComponent]

})
export class AppModule { }
