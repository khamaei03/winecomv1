import { AbstractControl } from '@angular/forms';

export function ValidateUrl(control: AbstractControl) {
  if (!control.value.startsWith('https') || !control.value.includes('.io')) {
    return { validUrl: true };
  }
  return null;
}

export function PasswordValidator(control: AbstractControl): { [key: string]: boolean } | null {
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');
  if (password.pristine || confirmPassword.pristine) {
    return null;
  }
  return password && confirmPassword && password.value !== confirmPassword.value ?
    { misMatch: true } :
    null;

}

export function NameValidator(control: AbstractControl): { [key: string]: boolean } | null {
  const fName = control.get('firstName');
  const lName = control.get('lastName');
  if (fName.pristine || lName.pristine) {
    return fName.pristine || lName.pristine ?
      { testFName: true } :
      null;
  }

}
