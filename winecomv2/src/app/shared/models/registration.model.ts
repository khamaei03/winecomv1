export class Register {
    firstName?: string;
    lastName?: string;
    password?: string;
    confirm?: string;
    email?: string;
}
