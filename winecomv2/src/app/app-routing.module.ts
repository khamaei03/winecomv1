import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorpageComponent } from './features/dashboard/errorpage/errorpage.component';


const routes: Routes = [

  { path: '',loadChildren: './features/dashboard/dashboard.module#DashboardModule'},
  { path: 'users', loadChildren: './features/users/users.module#UsersModule'},
  { path: 'images', loadChildren: './features/products/products.module#ProductsModule'},
  { path: '**', redirectTo: '/error404', pathMatch: 'full'},
  { path: 'error404', component: ErrorpageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

 /*  {
    path: '', redirectTo: 'images', pathMatch: 'full'}, */
 /*  {
    path: 'images', component: ImagesComponent,
    children: [
      {
        path: '',
        loadChildren: './features/products/products.module#ProductsModule'
      }
    ]
  },
  {
    path: 'list', component: ImageListComponent
  } */
